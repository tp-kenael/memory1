﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Memory
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Bouton leBoutton00;
        public Bouton leBoutton10;
        public Bouton leBoutton01;
        public Bouton leBoutton11;

        public Bouton[] les2Boutons;

        public int nbreClic = 0;
        public MainWindow()
        {
            InitializeComponent();

            les2Boutons = new Bouton[2];

            leBoutton00 = new Bouton();
            leBoutton00.leBouton = btn001;
            leBoutton00.laValeur = 1;

            leBoutton10 = new Bouton();
            leBoutton10.leBouton = btn101;
            leBoutton10.laValeur = 1;

            leBoutton01 = new Bouton();
            leBoutton01.leBouton = btn011;
            leBoutton01.laValeur = 2;

            leBoutton11 = new Bouton();
            leBoutton11.leBouton = btn111;
            leBoutton11.laValeur = 2;
        }

        public void Cliquer2Fois(Bouton leBouton)
        {
            nbreClic++;
            if (les2Boutons[0] == null)
            {
                les2Boutons[0] = leBouton;
                les2Boutons[0].leBouton.Visibility = Visibility.Visible;
            }
            else
            {
                if(les2Boutons[1] == null)
                {
                    les2Boutons[1] = leBouton;
                    les2Boutons[1].leBouton.Visibility = Visibility.Visible;
                    
                    if(les2Boutons[0].laValeur == les2Boutons[1].laValeur)
                    {
                        MessageBox.Show("C'est good! - Nombre de clic : " + nbreClic);
                        les2Boutons[0].leStatus = true;
                        les2Boutons[1].leStatus = true;
                    }
                    else
                    {
                        MessageBox.Show("RIP - Nombre de clic : " + nbreClic);
                        les2Boutons[1].leBouton.Visibility = Visibility.Hidden;
                        les2Boutons[0].leBouton.Visibility = Visibility.Hidden;
                    }
                    les2Boutons[1] = null;
                    les2Boutons[0] = null;
                }
            }
        }
        public bool Verification()
        {
            bool resultat = true;
            if (leBoutton00.leStatus == false)
            {
                resultat = false;
            }
            else if (leBoutton10.leStatus == false)
            {
                resultat = false;
            }
            else if (leBoutton01.leStatus == false)
            {
                resultat = false;
            }
            else if (leBoutton11.leStatus == false)
            {
                resultat = false;
            }
        }
        private void btn000_Click(object sender, RoutedEventArgs e)
        {
            Cliquer2Fois(leBoutton00);
        }
        private void btn100_Click(object sender, RoutedEventArgs e)
        {
            Cliquer2Fois(leBoutton10);
        }

        private void btn010_Click(object sender, RoutedEventArgs e)
        {
            Cliquer2Fois(leBoutton01);
        }

        private void btn110_Click(object sender, RoutedEventArgs e)
        {
            Cliquer2Fois(leBoutton11);
        }
    }
    public class Bouton
    {
        public Button leBouton;
        public int laValeur;
        public bool leStatus = false;
    }
}
