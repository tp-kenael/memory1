﻿using prototype.Model;
using Prototype.Controler;
using Prototype.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prototype.View
{
    /// <summary>
    /// Logique d'interaction pour VueJouer.xaml
    /// </summary>
    public partial class VueJouer : Window
    {
        private static string[] cheminLesImages;
        private string[] cheminLesImagesDuJeu;
        private Carte[] lesCartesModels;
        private Carte[] lesCartesJeu;
        private int nbreCarteModel = 2;
        private int nbreCarte = 4;
        private int nbreColRang = 0;
        private DoublonCarte[] lesDoublonsCartes;
        private bool raffraichir = false;

        public VueJouer()
        { 
            InitializeComponent();
            cheminLesImages = new string[19];
            for (int i = 0; i < 18; i++)
            {
                cheminLesImages[i] = "/Resources/image" + i + ".jpg";
            }

            //Mettre à disposition les images
            cheminLesImagesDuJeu = new string[nbreCarteModel + 1];
            if (cheminLesImages.Length != 0)
            {
                for (int i = 0; i < nbreCarteModel + 1; i++)
                {
                    cheminLesImagesDuJeu[i] = cheminLesImages[i];
                }
            }

            //Créer des cartesModel
            lesCartesModels = new Carte[nbreCarteModel];
            for (int i = 0; i < nbreCarteModel; i++)
            {
                lesCartesModels[i] = new Carte(cheminLesImagesDuJeu[0], cheminLesImagesDuJeu[i + 1]);
            }
            //Mettre à jour les doublons
            lesDoublonsCartes = new DoublonCarte[nbreCarteModel];
            for (int i = 0; i < nbreCarteModel; i++)
            {
                lesDoublonsCartes[i] = lesCartesModels[i].GetDoublonCarte();
            }

            Jouer.SetDoublonsDeCartes(lesDoublonsCartes);

            //Générer les cartes du jeu
            lesCartesJeu = new Carte[nbreCarte];
            for (int i = 0; i < nbreCarteModel; i++)
            {
                lesCartesJeu[i] = lesCartesModels[i].GetDoublonCarte().GetCarte1();
                lesCartesJeu[i + nbreCarteModel] = lesCartesModels[i].GetDoublonCarte().GetCarte2();
            }
            for (int i = 0; i < nbreCarteModel; i++)
            {
                GdFond.RowDefinition.Add(new RowDefinition());
                GdFond.ColumnDefinition.Add(new ColumnDefinition());
            }

            for (int i = 0; i < nbreCarte; i++)
            {
                GdFond.Children.Add(lesCartesJeu[i].GetBouton());
            }
            int k = 0;
            for (int i = 0; i < nbreColRang; i++)
            {
                for (int j = 0; j < nbreColRang; j++)
                {
                    Grid.SetRow(GdFond.Children[k], i);
                    Grid.SetColumn(GdFond.Children[k], j);
                    k++;
                }
            }

        }
        public void BoutonClick(Object sender, RoutedEventArgs e)
        {
            MessageBox.Show("ok");
            bool result;
            foreach (Carte elem in lesCartesJeu)
            {
                if (sender.Equals(elem.GetBouton()))
                {
                    result = Jouer.JouerUnTour(elem);
                    SetRaffraichir(result);
                }
            }
        }
        private void SetRaffraichir(bool leResultat)
        {
            this.raffraichir = leResultat;
            OnPropertyChanged("raffraichir");
        }

        protected void OnPropertyChanged(string propertyName)
        {
            //throw new NotImplementedException();
            MessageBox.Show("OK" + raffraichir);
            if (raffraichir) Jouer.VerifierUnTour();
        }
    }
}
