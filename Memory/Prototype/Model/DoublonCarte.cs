﻿using Prototype.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototype.Model
{
    public class DoublonCarte
    {
        private static int num = 0;
        private Carte carte1 = null;
        private Carte carte2 = null;
        private bool status = false;
        private int identifiant = 0;
        public DoublonCarte(Carte laCarte)
        {
            num++;
            identifiant = num;
            this.carte1 = new Carte(laCarte);
            this.carte2 = new Carte(laCarte);
            this.carte2.SetDoublonCarte(this);
            this.carte1.SetDoublonCarte(this);
        }

        public Carte GetCarte1()
        {
            return this.carte1;
        }
        public Carte GetCarte2()
        {
            return this.carte2;
        }
        public bool GetStatus()
        {
            return this.status;
        }
        public int GetIdentifiant()
        {
            return this.identifiant;
        }
        public void VerifierStatus(bool leStatus)
        {
            this.status=leStatus;
        }
        public string Description()
        {
            string resultat = "le status est : " + this.status + "\n l'identifiant est : " + this.identifiant;
            resultat += "\n la carte1 : " + this.carte1.Description() + "\n la carte2 : " + this.carte2.Description();
            return resultat;
        }
    }
}