﻿using prototype.Model;
using Prototype.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace Prototype.Model
{
    public class Carte
    {
        private static int nbreCarte = 0;
        private static int numero = 0;
        private DoublonCarte doublonCarte = null;
        private Button bouton = null;
        private bool status = false;
        private int valeur = 0;
        private string cheminImageDos = null;
        private string cheminImageFace = null;
        private Image imageDos = null;
        private Image imageFace = null;
        private string titre = null;
        private int numColFenetre = 0;
        private int numRangFenetre = 0;
        private int identifiant = 0;

        public Carte (string leCheminImageDos, string leCheminImageFace)
        {
            nbreCarte++;
            this.valeur = nbreCarte;
            this.cheminImageDos = leCheminImageDos;
            this.cheminImageFace = leCheminImageFace;
            this.titre = "Carte Modèle n°" + nbreCarte;
            this.doublonCarte = new DoublonCarte(this);
        }
        public Carte(Carte LaCarte)
        {
            numero++;
            this.identifiant = numero;
            this.imageDos = LesServices.CreerUneImage(LaCarte.GetCheminImageDos());
            this.imageFace = LesServices.CreerUneImage(LaCarte.GetCheminImageFace());
            this.status = LaCarte.GetStatus();
            this.bouton = new Button();
            this.AppareillerBouton();
        }

        public Button GetBouton()
        {
            return this.bouton;
        }
        public bool GetStatus()
        {
            return this.status;
        }
        public void SetStatus(bool leStatus)
        {
            this.status = leStatus;
        }
        public int GetValeur()
        {
            return this.valeur;
        }
        public DoublonCarte GetDoublonCarte()
        {
            return this.doublonCarte;
        } 
        public void SetDoublonCarte(DoublonCarte leDoublon)
        {
            this.doublonCarte = leDoublon;
        }
        public int GetIdentifiant()
        {
            return this.identifiant;
        }
        public int GetNumColFenetre()
        {
            return this.numColFenetre;
        }
        public void SetNumColFenetre(int numCol)
        {
            this.numColFenetre = numCol;
        }
        public int GetNumRangFenetre()
        {
            return this.numRangFenetre;
        }
        public void SetNumRangFenetre(int numRang)
        {
            this.numRangFenetre = numRang;
        }
        public string GetCheminImageDos()
        {
            return this.cheminImageDos;
        }
        public string GetCheminImageFace()
        {
            return this.cheminImageFace;
        }
        public void AppareillerBouton()
        {
            if (this.status == false)
            {
                this.bouton.Content = this.imageDos;/*Image*/;
            }
            else if (this.status==true)
            {
                this.bouton.Content = this.imageFace;/*Image*/;
            }
        }
        public string Description()
        {
            string resultat ="le status est : " + this.status + "\n le titre est : " + this.titre;
            resultat += "\n le chemin dos est : " + this.cheminImageDos + "\n le chemin face est : "+ cheminImageFace;
            resultat += "\n La valeur est : " + this.valeur+ "\nla source :"+((Image)this.bouton.Content).Source;
            return resultat;
        }
    }
}
